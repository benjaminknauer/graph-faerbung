import java.io.IOException;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class RemoteGraphRenderer extends GraphRenderer {

    private static final String REMOTE_RENDER_API_URL = "https://graphviz.benjaminknauer.de/dot?format=png";

    public byte[] renderGraph(Graph g) {
        String dotFormatedGraphRepresentation = generateDotFormatedGraph(g);
        try {
            return renderGraphUsingRemoteApi(dotFormatedGraphRepresentation);
        } catch (IOException | InterruptedException e) {
            System.out.println("Fehler beim Rendern des Graphen!");
            e.printStackTrace();
            return null;
        }
    }

    private byte[] renderGraphUsingRemoteApi(String dotFormatedGraphRepresentation) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(REMOTE_RENDER_API_URL))
                .POST(HttpRequest.BodyPublishers.ofString(dotFormatedGraphRepresentation))
                .build();

        HttpResponse<byte[]> response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());

        return response.body();
    }

}
