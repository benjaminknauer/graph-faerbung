public class GraphPrinter {

    public void print(Graph g) {
        List<Vertex> allVertices = g.getVertices();
        List<Edge> allEdges = g.getEdges();

        System.out.println("---------");
        System.out.println("Knoten: ");
        allVertices.toFirst();
        int vertexCount = 0;
        while (allVertices.hasAccess()) {
            Vertex currentVertex = allVertices.getContent();
            if (currentVertex instanceof ColorVertex) {
                System.out.println(currentVertex.getID() + " - Farbe: " + ((ColorVertex) currentVertex).getColor());
            } else {
                System.out.println(currentVertex.getID());
            }
            allVertices.next();
            vertexCount++;
        }
        System.out.println("\nAnzahl Knoten: " + vertexCount);
        System.out.println();

        System.out.println("Kanten:");
        int edgeCount = 0;
        while (allEdges.hasAccess()) {
            Edge currentEdge = allEdges.getContent();
            System.out.printf("%s ---%s--- %s%n", currentEdge.getVertices()[0].getID(), currentEdge.getWeight(), currentEdge.getVertices()[1].getID());
            allEdges.next();
            edgeCount++;
        }
        System.out.println("Anzahl Kanten: " + edgeCount);
        System.out.println("---------");
    }

}
