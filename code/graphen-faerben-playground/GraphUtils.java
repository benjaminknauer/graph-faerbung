public class GraphUtils {

    public static Graph deepCopy(Graph g) {
        if (!g.getVertices().getContent().getClass().getCanonicalName().equals(ColorVertex.class.getCanonicalName())) {
            throw new RuntimeException("Unsupported Vertex Class");
        }
        if (!g.getEdges().getContent().getClass().getCanonicalName().equals(Edge.class.getCanonicalName())) {
            throw new RuntimeException("Unsupported Edge Class");
        }

        Graph copyGraph = new Graph();

        List<Vertex> vertices = g.getVertices();
        List<Edge> edges = g.getEdges();

        vertices.toFirst();
        while (vertices.hasAccess()) {
            ColorVertex v = (ColorVertex) vertices.getContent();
            ColorVertex nV = new ColorVertex(v.getID());
            nV.setColor(v.getColor());
            nV.setMark(v.isMarked());
            copyGraph.addVertex(nV);

            vertices.next();
        }

        edges.toFirst();
        while (edges.hasAccess()) {
            Edge e = edges.getContent();
            Vertex vA = e.getVertices()[0];
            Vertex vB = e.getVertices()[1];

            Vertex nVA = copyGraph.getVertex(vA.getID());
            Vertex nVB = copyGraph.getVertex(vB.getID());

            Edge nE = new Edge(nVA, nVB, e.getWeight());
            copyGraph.addEdge(nE);

            edges.next();
        }

        return copyGraph;
    }

}
