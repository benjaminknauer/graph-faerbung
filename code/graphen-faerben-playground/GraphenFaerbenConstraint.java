import graphviewer.ViewerFrame;

public class GraphenFaerbenConstraint {

    private static final String[] FARBEN = {"green", "pink", "yellow", "blue", "purple"};

    private final GraphRenderer gr;
    private final ViewerFrame vf;
    private final GraphPrinter gp;

    public GraphenFaerbenConstraint(String title) {
        gp = new GraphPrinter();

        gr = new LocalGraphRenderer();
        //gr.setEnabled(false); // zum Rendern auskommentieren

        vf = gr.createWindow(title);
    }

    /**
     * Versucht einen Graphen mit der Anzahl der übergebenen Farben zu färben.
     * Hierfür wird ein Backtracking-Algorithmus eingesetzt. Dieser testet systematisch alle möglichen Kombinationen.
     *
     * @param g              zu färbender Graph
     * @param maximaleFarben Anzahl der maximal zu verwendenden Farben
     */
    public void graphFaerben(Graph g, int maximaleFarben) {
        List<Vertex> allVerticesList = g.getVertices();
        allVerticesList.toFirst();
        ColorVertex startknoten = (ColorVertex) allVerticesList.getContent();

        boolean success = graphFaerbenRecursiv(g, allVerticesList, startknoten, maximaleFarben);
        if (success) {
            System.out.println("Erfolg! Es wurde eine korrekt gefärbter Graph gefunden.");
            System.out.println();
            gp.print(g);
            gr.renderGraphAndShow(vf, g, 0);
        } else {
            System.out.println("Fehlschlag! Es wurde kein korrekt gefärbter Graph gefunden.");
        }
    }

    /**
     * Interne Backtracking-Methode zum systematischen Testen der Färbungen.
     *
     * @param g                  zu färbender Graph
     * @param alleKnoten         Array mit allen Vertices des Graphen
     * @param currentVertexIndex Index des aktuell zu betrachtender Vertex in alleKnoten
     * @param maximaleFarben     maximale Anzahl der zu verwendenden Farben
     * @return true, falls ein korrekt gefärbter Graph gefunden wurde. false, sonst.
     */
    private boolean graphFaerbenRecursiv(Graph g, List<Vertex> alleKnoten, ColorVertex aktuellerKnoten, int maximaleFarben) {
        for (int i = 0; i < maximaleFarben; i++) {
            String aktuelleFarbe = FARBEN[i];
            aktuellerKnoten.setColor(aktuelleFarbe);
            aktuellerKnoten.setMark(true);
            gr.renderGraphAndShow(vf, g, 0);
            if (isFarbeMoeglich(g, aktuellerKnoten)) {
                if (g.allVerticesMarked()) {
                    return true; // Färbung gefunden!
                } else {
                    ColorVertex nextVertex = getNextVertex(alleKnoten, aktuellerKnoten);
                    boolean faerbungGefunden = graphFaerbenRecursiv(g, alleKnoten, nextVertex, maximaleFarben);
                    if (faerbungGefunden) {
                        return true; // Färbung gefunden!
                    }
                }
            }
            // Backtracking: Letzte Färbung rückgängig machen
            aktuellerKnoten.setColor(null);
            aktuellerKnoten.setMark(false);
            gr.renderGraphAndShow(vf, g, 0);
        }

        return false; // Keine Färbung gefunden!
    }

    private ColorVertex getNextVertex(List<Vertex> allVertices, ColorVertex v) {
        allVertices.toFirst();
        while (allVertices.getContent() != v) {
            allVertices.next();
        }
        allVertices.next();
        ColorVertex nextVertex = (ColorVertex) allVertices.getContent();
        return nextVertex;
    }

    private boolean isFarbeMoeglich(Graph g, ColorVertex colorVertex) {
        List<Vertex> neighbours = g.getNeighbours(colorVertex);
        boolean gueltig = true;
        neighbours.toFirst();
        while (neighbours.hasAccess()) {
            ColorVertex aktuellerNachbar = (ColorVertex) neighbours.getContent();
            if (colorVertex.getColor().equals(aktuellerNachbar.getColor())) {
                gueltig = false;
            }
            neighbours.next();
        }
        return gueltig;
    }

}
