import graphviewer.ViewerFrame;

public class GraphenAlleFaerbungen {

    private static final String[] FARBEN = {"green", "pink", "yellow", "blue", "purple"};

    private final GraphRenderer gr;
    private final ViewerFrame vf;
    private final GraphPrinter gp;

    private int anzahlFaerbungen = 0;
    private final List<Graph> gefundeneFaerbungen = new List<>();

    public GraphenAlleFaerbungen(String title) {
        gp = new GraphPrinter();

        gr = new LocalGraphRenderer();
        //gr.setEnabled(false); // zum Rendern auskommentieren

        vf = gr.createWindow(title);
    }

    /**
     * Versucht einen Graphen mit der Anzahl der übergebenen Farben zu färben.
     * Hierfür wird ein Backtracking-Algorithmus eingesetzt. Dieser testet systematisch alle möglichen Kombinationen.
     *
     * @param g              zu färbender Graph
     * @param maximaleFarben Anzahl der maximal zu verwendenden Farben
     */
    public void graphFaerben(Graph g, int maximaleFarben) {
        List<Vertex> allVerticesList = g.getVertices();
        ColorVertex[] allVerticesArray = ListUtils.toArray(allVerticesList, ColorVertex.class);

        graphFaerbenRecursiv(g, allVerticesArray, 0, maximaleFarben);
        System.out.printf("Es wurden %s Färbungen gefunden.", anzahlFaerbungen);

        int aktuelleFaerbung = 0;
        gefundeneFaerbungen.toFirst();
        while (gefundeneFaerbungen.hasAccess()) {
            gr.renderGraphAndShow(gefundeneFaerbungen.getContent(), "Färbung: " + aktuelleFaerbung);
            aktuelleFaerbung++;
            gefundeneFaerbungen.next();
        }
    }

    /**
     * Interne Backtracking-Methode zum systematischen Testen der Färbungen.
     *
     * @param g                  zu färbender Graph
     * @param allVertices        Array mit allen Vertices des Graphen
     * @param currentVertexIndex Index des aktuell zu betrachtender Vertex in allVertices
     * @param maximaleFarben     maximale Anzahl der zu verwendenden Farben
     */
    private void graphFaerbenRecursiv(Graph g, ColorVertex[] allVertices, int currentVertexIndex, int maximaleFarben) {
        if (currentVertexIndex < allVertices.length) {
            ColorVertex v = allVertices[currentVertexIndex];
            for (int i = 0; i < maximaleFarben; i++) {
                String aktuelleFarbe = FARBEN[i];
                v.setColor(aktuelleFarbe);
                gr.renderGraphAndShow(vf, g, 0);
                if (isFarbeMoeglich(g, v)) {
                    graphFaerbenRecursiv(g, allVertices, currentVertexIndex + 1, maximaleFarben);
                }
                v.setColor(null); // Backtracking: Letzte Färbung rückgängig machen
                gr.renderGraphAndShow(vf, g, 0);
            }
        } else {
            System.out.println("Lösung gefunden");
            gp.print(g);
            // Färbung gefunden!
            anzahlFaerbungen++;
            gefundeneFaerbungen.append(GraphUtils.deepCopy(g));
        }
    }

    private boolean isFarbeMoeglich(Graph g, ColorVertex colorVertex) {
        List<Vertex> neighbours = g.getNeighbours(colorVertex);
        boolean gueltig = true;
        neighbours.toFirst();
        while (neighbours.hasAccess()) {
            ColorVertex aktuellerNachbar = (ColorVertex) neighbours.getContent();
            if (colorVertex.getColor().equals(aktuellerNachbar.getColor())) {
                gueltig = false;
            }
            neighbours.next();
        }
        return gueltig;
    }

}
