public class GraphFactory {

    public Graph createAustralienGraph() {
        Graph g = new Graph();

        ColorVertex westernAustralia = new ColorVertex("Western Australia");
        ColorVertex northernTerritory = new ColorVertex("Northern Territory");
        ColorVertex southAustralia = new ColorVertex("South Australia");
        ColorVertex queensland = new ColorVertex("Queensland");
        ColorVertex newSouthWales = new ColorVertex("New South Wales");
        ColorVertex victoria = new ColorVertex("Victoria");
        ColorVertex tasmania = new ColorVertex("Tasmania");

        java.util.List.of(westernAustralia, northernTerritory, southAustralia, queensland, newSouthWales, victoria, tasmania)
                .forEach(g::addVertex);

        java.util.List.of(
                new Edge(westernAustralia, northernTerritory, 0),
                new Edge(westernAustralia, southAustralia, 0),
                new Edge(northernTerritory, southAustralia, 0),
                new Edge(northernTerritory, queensland, 0),
                new Edge(southAustralia, queensland, 0),
                new Edge(southAustralia, newSouthWales, 0),
                new Edge(southAustralia, victoria, 0),
                new Edge(queensland, newSouthWales, 0),
                new Edge(newSouthWales, victoria, 0)
        ).forEach(g::addEdge);

        return g;
    }

    public Graph createAustralienGraphWithoutTasmania() {
        Graph g = new Graph();

        ColorVertex westernAustralia = new ColorVertex("Western Australia");
        ColorVertex northernTerritory = new ColorVertex("Northern Territory");
        ColorVertex southAustralia = new ColorVertex("South Australia");
        ColorVertex queensland = new ColorVertex("Queensland");
        ColorVertex newSouthWales = new ColorVertex("New South Wales");
        ColorVertex victoria = new ColorVertex("Victoria");

        java.util.List.of(westernAustralia, northernTerritory, southAustralia, queensland, newSouthWales, victoria)
                .forEach(g::addVertex);

        java.util.List.of(
                new Edge(westernAustralia, northernTerritory, 0),
                new Edge(westernAustralia, southAustralia, 0),
                new Edge(northernTerritory, southAustralia, 0),
                new Edge(northernTerritory, queensland, 0),
                new Edge(southAustralia, queensland, 0),
                new Edge(southAustralia, newSouthWales, 0),
                new Edge(southAustralia, victoria, 0),
                new Edge(queensland, newSouthWales, 0),
                new Edge(newSouthWales, victoria, 0)
        ).forEach(g::addEdge);

        return g;
    }


    public Graph createAustralienGraphSmall() {
        Graph g = new Graph();

        ColorVertex westernAustralia = new ColorVertex("Western Australia");
        ColorVertex northernTerritory = new ColorVertex("Northern Territory");
        ColorVertex southAustralia = new ColorVertex("South Australia");

        java.util.List.of(westernAustralia, northernTerritory, southAustralia).forEach(g::addVertex);

        java.util.List.of(
                new Edge(westernAustralia, northernTerritory, 0),
                new Edge(westernAustralia, southAustralia, 0),
                new Edge(northernTerritory, southAustralia, 0)
        ).forEach(g::addEdge);

        return g;
    }

}
