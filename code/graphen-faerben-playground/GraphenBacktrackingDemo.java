public class GraphenBacktrackingDemo {

    private final GraphFactory graphFactory = new GraphFactory();

    public void australienFaerben() {
        Graph g = graphFactory.createAustralienGraph();
        int farbenCount = 3;
        GraphenFaerben gf = new GraphenFaerben("Graph mit " + farbenCount + " Farben färben");
        gf.graphFaerben(g, farbenCount);
    }

    public void australienFaerbenConstraint() {
        Graph g = graphFactory.createAustralienGraph();
        int farbenCount = 3;
        GraphenFaerbenConstraint gfc = new GraphenFaerbenConstraint("Graph mit " + farbenCount + " Farben färben");
        gfc.graphFaerben(g, farbenCount);
    }

    public void australienAlleFaerbungen() {
        Graph g = graphFactory.createAustralienGraphWithoutTasmania();
        int farbenCount = 3;
        GraphenAlleFaerbungen gfg = new GraphenAlleFaerbungen("Graph mit " + farbenCount + " Farben färben");
        gfg.graphFaerben(g, farbenCount);
    }

    public void australienFaerbenGreedy() {
        Graph g = graphFactory.createAustralienGraphWithoutTasmania();
        int farbenCount = 3;
        GraphenFaerbungenGreedy gfg = new GraphenFaerbungenGreedy("Graph mit " + farbenCount + " Farben färben");
        gfg.graphFaerben(g, farbenCount);
    }

    public static void main(String[] args) {
        GraphenBacktrackingDemo gaD = new GraphenBacktrackingDemo();
        //gaD.australienFaerben();
        gaD.australienFaerbenConstraint();
        //gaD.australienAlleFaerbungen();
        //gaD.australienFaerbenGreedy();
    }

}
