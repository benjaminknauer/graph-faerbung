import graphviewer.ViewerFrame;

import java.util.HashSet;
import java.util.Set;

public class GraphenFaerbungenGreedy {

    private static final String[] FARBEN = {"green", "pink", "yellow", "blue", "purple"};

    private final GraphRenderer gr;
    private final ViewerFrame vf;
    private final GraphPrinter gp;

    public GraphenFaerbungenGreedy(String title) {
        gp = new GraphPrinter();

        gr = new LocalGraphRenderer();
        //gr.setEnabled(false); // zum Rendern auskommentieren

        vf = gr.createWindow(title);
    }

    /**
     * Versucht einen Graphen mit der Anzahl der übergebenen Farben zu färben.
     * Hierfür wird ein Backtracking-Algorithmus eingesetzt. Dieser testet systematisch alle möglichen Kombinationen.
     *
     * @param g              zu färbender Graph
     * @param maximaleFarben Anzahl der maximal zu verwendenden Farben
     */
    public void graphFaerben(Graph g, int maximaleFarben) {
        String[] verfuegbareFarben = new String[maximaleFarben];
        System.arraycopy(FARBEN, 0, verfuegbareFarben, 0, maximaleFarben);
        ColorVertex[] alleKnoten = ListUtils.toArray(g.getVertices(), ColorVertex.class);
        Set<ColorVertex> ungefaerbteKnoten = new HashSet<>(java.util.List.of(alleKnoten));

        graphenFaerbenGreedy(g, verfuegbareFarben, ungefaerbteKnoten);
    }

    private void graphenFaerbenGreedy(Graph g, String[] verfuegbareFarben, Set<ColorVertex> ungefaerbteKnoten) {
        while (!ungefaerbteKnoten.isEmpty()) {
            ColorVertex aktuellerKnoten = ungefaerbteKnoten.stream().findFirst().orElseThrow();
            Set<String> erlaubteFarben = calculateErlaubteFarben(g, aktuellerKnoten, verfuegbareFarben);
            if (erlaubteFarben.isEmpty()) {
                System.out.println("Fehler! Sackgasse: Keine weitere Färbung möglich.");
                return;
            } else {
                String farbe = erlaubteFarben.stream().findFirst().orElseThrow();
                aktuellerKnoten.setColor(farbe);
                ungefaerbteKnoten.remove(aktuellerKnoten);
                gr.renderGraphAndShow(vf, g, 0);
            }
        }

        System.out.println("Färbung gefunden!");
        gp.print(g);
    }

    private Set<String> calculateErlaubteFarben(Graph g, ColorVertex v, String[] verfuegbareFarben) {
        Set<String> erlaubteFarben = new HashSet<>(java.util.List.of(verfuegbareFarben));

        List<Vertex> neighbours = g.getNeighbours(v);
        neighbours.toFirst();
        while (neighbours.hasAccess()) {
            ColorVertex currentNeighbour = (ColorVertex) neighbours.getContent();
            if (currentNeighbour.getColor() != null) {
                erlaubteFarben.remove(currentNeighbour.getColor());
            }
            neighbours.next();
        }

        return erlaubteFarben;
    }

}
