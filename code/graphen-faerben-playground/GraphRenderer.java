import graphviewer.ViewerFrame;

import javax.swing.*;

public abstract class GraphRenderer {

    private boolean enabled = true;

    abstract byte[] renderGraph(Graph g);

    public void renderGraphAndShow(Graph g, String title) {
        if (!enabled) {
            return;
        }
        byte[] imgData = renderGraph(g);
        ViewerFrame vf = createWindow(title);
        showGraphImage(vf, imgData);

        ListUtils.size(new List<>());
    }

    public void renderGraphAndShow(ViewerFrame vf, Graph g, int delayInMs) {
        if (!enabled) {
            return;
        }
        byte[] imgData = renderGraph(g);
        try {
            Thread.sleep(delayInMs);
        } catch (InterruptedException ignored) {
        }
        showGraphImage(vf, imgData);
    }

    public ViewerFrame createWindow(String title) {
        if (!enabled) {
            return null;
        }
        ViewerFrame vf = new ViewerFrame(title);
        vf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vf.setLocationByPlatform(true);
        vf.setVisible(true);

        return vf;
    }

    private void showGraphImage(ViewerFrame vf, byte[] imgData) {
        vf.loadImage(imgData);
        vf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        if (!vf.isVisible()) {
            vf.setLocationByPlatform(true);
            vf.setVisible(true);
        }
    }

    String generateDotFormatedGraph(Graph g) {
        StringBuilder sb = new StringBuilder();
        List<Edge> edges = g.getEdges();

        sb.append("graph { \n");

        List<Vertex> vertices = g.getVertices();
        vertices.toFirst();
        while (vertices.hasAccess()) {
            Vertex currentVertex = vertices.getContent();
            if (currentVertex instanceof ColorVertex) {
                sb.append(String.format(" \"%s\" [style=\"filled\", fillcolor=\"%s\"]%n",
                        currentVertex.getID(),
                        calcFillcolor((ColorVertex) currentVertex))
                );
            }
            sb.append(String.format(" \"%s\" [color=\"%s\"]%n", currentVertex.getID(), calcColor(currentVertex)));
            vertices.next();
        }
        sb.append(String.format("%n"));

        edges.toFirst();
        while (edges.hasAccess()) {
            Edge currentEdge = edges.getContent();
            Vertex vertexA = currentEdge.getVertices()[0];
            Vertex vertexB = currentEdge.getVertices()[1];
            String edgeRepresentationInDotFormat = String.format(
                    "   \"%s\" -- \"%s\" [weight=%s, label=%s, color=%s]%n",
                    vertexA.getID(),
                    vertexB.getID(),
                    currentEdge.getWeight(),
                    currentEdge.getWeight(),
                    calcColor(currentEdge)
            );
            sb.append(edgeRepresentationInDotFormat);
            edges.next();
        }

        sb.append("}");

        return sb.toString();
    }

    private String calcColor(Edge e) {
        return e.isMarked() ? "red" : "black";
    }

    private String calcColor(Vertex v) {
        return v.isMarked() ? "red" : "black";
    }

    private String calcFillcolor(ColorVertex v) {
        return v.getColor() != null ? v.getColor() : "white";
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean pEnabled) {
        enabled = pEnabled;
    }
}
