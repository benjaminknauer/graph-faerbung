import java.lang.reflect.Array;

public class ListUtils {

    public static <ContentType> int size(List<ContentType> list) {
        int size = 0;
        list.toFirst();
        while (list.hasAccess()) {
            size++;
            list.next();
        }

        return size;
    }

    public static <ContentType> ContentType[] toArray(List<?> list, Class<ContentType> clazz) {
        int listSize = size(list);
        ContentType[] array = (ContentType[]) Array.newInstance(clazz, listSize);
        list.toFirst();
        int idx = 0;
        while (list.hasAccess()) {
            array[idx] = (ContentType) list.getContent();
            idx++;
            list.next();
        }

        return array;
    }

}
