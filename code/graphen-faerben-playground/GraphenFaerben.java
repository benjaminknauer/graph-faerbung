import graphviewer.ViewerFrame;

public class GraphenFaerben {

    private static final String[] FARBEN = {"green", "pink", "yellow", "blue", "purple"};

    private final GraphRenderer gr;
    private final ViewerFrame vf;
    private final GraphPrinter gp;

    public GraphenFaerben(String title) {
        gp = new GraphPrinter();

        gr = new LocalGraphRenderer();
        //gr.setEnabled(false); // zum Rendern auskommentieren

        vf = gr.createWindow(title);
    }

    /**
     * Versucht einen Graphen mit der Anzahl der übergebenen Farben zu färben.
     * Hierfür wird ein Backtracking-Algorithmus eingesetzt. Dieser testet systematisch alle möglichen Kombinationen.
     *
     * @param g              zu färbender Graph
     * @param maximaleFarben Anzahl der maximal zu verwendenden Farben
     */
    public void graphFaerben(Graph g, int maximaleFarben) {
        List<Vertex> allVerticesList = g.getVertices();
        ColorVertex[] allVerticesArray = ListUtils.toArray(allVerticesList, ColorVertex.class);

        boolean success = graphFaerbenRecursiv(g, allVerticesArray, 0, maximaleFarben);
        if (success) {
            System.out.println("Erfolg! Es wurde eine korrekt gefärbter Graph gefunden.");
            System.out.println();
            gp.print(g);
            gr.renderGraphAndShow(vf, g, 0);
        } else {
            System.out.println("Fehlschlag! Es wurde kein korrekt gefärbter Graph gefunden.");
        }
    }

    /**
     * Interne Backtracking-Methode zum systematischen Testen der Färbungen.
     *
     * @param g                  zu färbender Graph
     * @param allVertices        Array mit allen Vertices des Graphen
     * @param currentVertexIndex Index des aktuell zu betrachtender Vertex in allVertices
     * @param maximaleFarben     maximale Anzahl der zu verwendenden Farben
     * @return true, falls ein korrekt gefärbter Graph gefunden wurde. false, sonst.
     */
    private boolean graphFaerbenRecursiv(Graph g, ColorVertex[] allVertices, int currentVertexIndex, int maximaleFarben) {
        // Rekursionsanker:
        // Fall alle Vertices durchlaufen sind
        //      prüfe, ob der Graph korrekt gefärbt ist und gib die Antwort zurück.
        if (currentVertexIndex == allVertices.length) {
            return isGraphKorrektGefaerbt(g);
        }

        ColorVertex currentVertex = allVertices[currentVertexIndex];

        // Färbe den aktuellen Vertex nacheinander in allen möglichen Farben.
        for (int i = 0; i < maximaleFarben; i++) {

            // Färbe den Vertex mit der aktuellen Farbe.
            String currentFarbe = FARBEN[i];
            currentVertex.setColor(currentFarbe);
            currentVertex.setMark(true);

            // Zeige den aktuellen Graphen
            gr.renderGraphAndShow(vf, g, 0); // um das Rendering zu beschleunigen auskommentiert.

            // Rufe den Algorithmus rekursiv für den nächsten Vertex im Array auf.
            boolean success = graphFaerbenRecursiv(g, allVertices, currentVertexIndex + 1, maximaleFarben);

            if (success) {
                // Falls ein korrekt gefärbter Graph gefunden wurde,
                //      brich ab und gib true zurück.
                return true;
            } else {
                // Sonst
                //      mache den letzten Schritt rückgängig. (Backtracking)
                currentVertex.setColor(null);
                currentVertex.setMark(false);

                // Zeige den aktuellen Graphen (nach Backtracking)
                //gr.renderGraphAndShow(vf, g, 0); // um das Rendering zu beschleunigen auskommentiert.
            }

        }

        // Es wurde keine gültige Färbung für den Graphen gefunden. Gib false zurück.
        return false;
    }

    /**
     * Testet, ob der übergebene Graph korrekt gefärbt ist.
     * Hierbei dürfen Knoten, die über eine Kante verbunden sind nicht dieselbe Farbe verwenden.
     *
     * @param g zu testender Graph
     * @return true, falls der Graph korrekt gefärbt ist. false, sonst.
     */
    private boolean isGraphKorrektGefaerbt(Graph g) {
        List<Edge> edges = g.getEdges();
        edges.toFirst();
        while (edges.hasAccess()) {
            Edge currentEdge = edges.getContent();
            ColorVertex vertexA = (ColorVertex) currentEdge.getVertices()[0];
            ColorVertex vertexB = (ColorVertex) currentEdge.getVertices()[1];

            String colorA = vertexA.getColor();
            String colorB = vertexB.getColor();

            if (colorA != null && colorA.equals(colorB)) {
                return false;
            }
            edges.next();
        }
        return true;
    }

}
