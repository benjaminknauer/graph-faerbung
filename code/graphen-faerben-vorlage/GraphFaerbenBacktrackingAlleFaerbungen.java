import graphviewer.ViewerFrame;

public class GraphFaerbenBacktrackingAlleFaerbungen {

    private static final String[] FARBEN = {"green", "pink", "yellow", "blue", "purple"};

    private final GraphRenderer gr;
    private final ViewerFrame vf;
    private final GraphPrinter gp;

    private int anzahlFaerbungen = 0;
    private final List<Graph> gefundeneFaerbungen = new List<>();

    /**
     * Konstruktor der Algorithmus-Klasse.
     *
     * @param title Titel, der für das Fenster ViewerFrame vf gesetzt wird.
     */
    public GraphFaerbenBacktrackingAlleFaerbungen(String title) {
        gp = new GraphPrinter();

        gr = new LocalGraphRenderer();
        //gr.setEnabled(false); // zum Rendern auskommentieren

        vf = gr.createWindow(title);
    }

    /**
     * Versucht einen Graphen mit der Anzahl k der übergebenen Farben zu färben.
     * Hierfür wird ein Backtracking-Algorithmus eingesetzt. Dieser testet systematisch alle möglichen Kombinationen.
     *
     * @param g              zu färbender Graph
     * @param maximaleFarben Anzahl der maximal zu verwendenden Farben
     */
    public void graphFaerben(Graph g, int maximaleFarben) {
        List<Vertex> allVerticesList = g.getVertices();
        allVerticesList.toFirst();
        ColorVertex startknoten = (ColorVertex) allVerticesList.getContent();

        graphFaerbenRekursiv(g, allVerticesList, startknoten, maximaleFarben);
        System.out.printf("Es wurden %s Färbungen gefunden.%n", anzahlFaerbungen);

        // TODO hier alle gefundenen Graphen anzeigen

    }

    /**
     * Interne Backtracking-Methode zum systematischen Testen der Färbungen.
     * Alle gefundenen Färbungen werden in der Liste gefundeneFaerbungen gespeichert.
     *
     * @param g               zu färbender Graph
     * @param alleKnoten      Liste mit allen Knoten des Graphen
     * @param aktuellerKnoten aktuell zu betrachtender Knoten aus alleKnoten
     * @param maximaleFarben  maximale Anzahl k der zu verwendenden Farben
     */
    private void graphFaerbenRekursiv(Graph g, List<Vertex> alleKnoten, ColorVertex aktuellerKnoten, int maximaleFarben) {

        // TODO Implementiert hier den Algorithmus
        throw new RuntimeException("Noch nicht implementiert");

    }

    /**
     * Gibt den nachfolgenden Knoten zum übergebenen Knoten v aus der Liste der Knoten zurück.
     *
     * @param allVertices Liste aller Knoten
     * @param v           Knoten, für den der Nachfolger gesucht wird
     * @return Nachfolger von v. Fall nicht vorhanden null.
     */
    private ColorVertex getNextVertex(List<Vertex> allVertices, ColorVertex v) {

        // TODO Implementiert hier ggfs. eine Hilfsmethode zum Finden es nächsten aktuellen Knotens
        throw new RuntimeException("Noch nicht implementiert");

    }

    /**
     * Prüft, ob der übergebene Knoten v eine gültige Färbung besitzt.
     *
     * @param g zu förbender Graph
     * @param v zu prüfender Knoten
     * @return true, falls Färbung gültig. false, sonst.
     */
    private boolean isFarbeMoeglich(Graph g, ColorVertex v) {

        // TODO Implementiert hier ggfs. eine Hilfsmethode zum Prüfen der Färbung des aktuellen Knotens
        throw new RuntimeException("Noch nicht implementiert");

    }

}
