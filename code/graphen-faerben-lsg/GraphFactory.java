public class GraphFactory {

    /**
     * Erzeugt einen Graphen zur Repräsentation der australischen Bundesstaaten/Provinzen.
     *
     * @return Australien-Graph
     */
    public Graph createAustralienGraph() {
        Graph g = new Graph();

        ColorVertex westaustralien = new ColorVertex("Westaustralien");
        ColorVertex northernTerritory = new ColorVertex("Northern Territory");
        ColorVertex suedaustralien = new ColorVertex("Südaustralien");
        ColorVertex queensland = new ColorVertex("Queensland");
        ColorVertex newSouthWales = new ColorVertex("New South Wales");
        ColorVertex victoria = new ColorVertex("Victoria");
        ColorVertex tasmania = new ColorVertex("Tasmania");

        java.util.List.of(westaustralien, northernTerritory, suedaustralien, queensland, newSouthWales, victoria, tasmania)
                .forEach(g::addVertex);

        java.util.List.of(
                new Edge(westaustralien, northernTerritory, 0),
                new Edge(westaustralien, suedaustralien, 0),
                new Edge(northernTerritory, suedaustralien, 0),
                new Edge(northernTerritory, queensland, 0),
                new Edge(suedaustralien, queensland, 0),
                new Edge(suedaustralien, newSouthWales, 0),
                new Edge(suedaustralien, victoria, 0),
                new Edge(queensland, newSouthWales, 0),
                new Edge(newSouthWales, victoria, 0)
        ).forEach(g::addEdge);

        return g;
    }

}
