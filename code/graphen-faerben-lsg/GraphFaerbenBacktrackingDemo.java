public class GraphFaerbenBacktrackingDemo {

    private final GraphFactory graphFactory = new GraphFactory();

    public void australienFaerben() {
        Graph g = graphFactory.createAustralienGraph();
        int farbenCount = 4;
        GraphFaerbenBacktracking gfc = new GraphFaerbenBacktracking("Graph mit " + farbenCount + " Farben färben");
        gfc.graphFaerben(g, farbenCount);
    }

    public void australienFaerbenAlleFaerbunge() {
        Graph g = graphFactory.createAustralienGraph();
        int farbenCount = 3;
        GraphFaerbenBacktrackingAlleFaerbungen gfbaf = new GraphFaerbenBacktrackingAlleFaerbungen("Graph mit " + farbenCount + " Farben färben");
        gfbaf.graphFaerben(g, farbenCount);
    }

    public static void main(String[] args) {
        GraphFaerbenBacktrackingDemo gad = new GraphFaerbenBacktrackingDemo();
        gad.australienFaerben();
        //gad.australienFaerbenAlleFaerbunge();
    }

}
