/**
 * Unterklasse der Klasse {@link Vertex}.
 * Verwaltet das zusätzliche Attribut String color.
 */
public class ColorVertex extends Vertex {

    private String color;

    /**
     * Ein neues Objekt vom Typ Vertex wird erstellt. Seine Markierung hat den Wert false.
     *
     * @param pID
     */
    public ColorVertex(String pID) {
        super(pID);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String pColor) {
        color = pColor;
    }
}
