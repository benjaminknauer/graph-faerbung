import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Implementierung der abstrakten Klasse {@link GraphRenderer}.
 * Verwendet die Graphviz-Bibliothek zum Rendern von Graphen.
 * Die Binary-Files müssen sich an den in den Konstanten definierten Pfaden befinden.
 */
public class LocalGraphRenderer extends GraphRenderer {

    private static final String BIN_PATH_GRAPHVIZ_MACOSX = "./graphviz/macosx/bin/dot";
    private static final String BIN_PATH_GRAPHVIZ_WINDOWS = ".\\graphviz\\windows\\bin\\dot.exe";
    private static final String GRAPHVIZ_ARGS = "-Tpng";
    private static final String OS = System.getProperty("os.name").toLowerCase();

    public byte[] renderGraph(Graph g) {
        String dotFormatedGraphRepresentation = generateDotFormatedGraph(g);
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (isMac()) {
            processBuilder.command(BIN_PATH_GRAPHVIZ_MACOSX, GRAPHVIZ_ARGS);
        } else if (isWindows()) {
            processBuilder.command(BIN_PATH_GRAPHVIZ_WINDOWS, GRAPHVIZ_ARGS);
        } else {
            throw new RuntimeException("Unsupported OS: " + OS);
        }

        try {
            Process dotProcess = processBuilder.start();

            InputStream itsOutput = dotProcess.getInputStream();
            OutputStream itsInput = dotProcess.getOutputStream();

            itsInput.write(dotFormatedGraphRepresentation.getBytes(StandardCharsets.UTF_8));
            itsInput.flush();
            itsInput.close();

            return itsOutput.readAllBytes();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean isWindows() {
        return OS.contains("win");
    }

    private boolean isMac() {
        return OS.contains("mac");
    }

}
