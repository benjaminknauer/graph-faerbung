package graphviewer;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class ImageComponent extends JPanel {
    private static final double MAX_ZOOM = 10;
    private static final double MIN_ZOOM = 0.2;

    private Dimension size = new Dimension(600, 400);
    private double scale;
    private double posX;
    private double posY;
    private Image img;

    public ImageComponent() {
        addKeyListener(new ScaleEvent());
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        setBackground(Color.BLACK);

        if (isImageLoaded()) {
            MediaTracker mt = new MediaTracker(this);
            mt.addImage(img, 0);
            try {
                mt.waitForAll();
            } catch (InterruptedException ignored) {
            }

            calcImage();
            g.drawImage(img, (int) posX, (int) posY, size.width, size.height, this);
        }
    }

    public void calcImage() {
        double imgWidth = img.getWidth(this) * scale;
        double imgHeight = img.getHeight(this) * scale;

        posX = getWidth() / 2.0 - imgWidth / 2;
        posY = getHeight() / 2.0 - imgHeight / 2;
        size = new Dimension((int) imgWidth, (int) imgHeight);
    }

    public void loadImage(String path, Dimension size) {
        setSize(size);
        img = new ImageIcon(path).getImage();

        int imageWidth = img.getWidth(this);
        if (imageWidth > getWidth())
            scale = (double) getWidth() / (double) imageWidth;
        else scale = 1.0;
    }

    public void loadImage(byte[] imgData, Dimension size) {
        setSize(size);
        img = new ImageIcon(imgData).getImage();

        int imageWidth = img.getWidth(this);
        if (imageWidth > getWidth())
            scale = (double) getWidth() / (double) imageWidth;
        else scale = 1.0;
    }

    private void scaleImage(double newScale) {
        scale = newScale;
        revalidate();
        repaint();
    }

    public boolean isImageLoaded() {
        return img != null;
    }

    public Dimension getPreferredSize() {
        return size;
    }

    private class ScaleEvent extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            if (isImageLoaded() && e.isControlDown()) {
                System.out.println("SHIFT");
                System.out.println(e.getKeyCode());
                if (e.getKeyCode() == 107) { // Plus-Key
                    if (scale <= MAX_ZOOM)
                        scaleImage(scale + 0.1);
                } else if (e.getKeyCode() == 109) { // Minus-Key
                    if (scale >= MIN_ZOOM)
                        scaleImage(scale - 0.1);
                }
            }
        }
    }
}
