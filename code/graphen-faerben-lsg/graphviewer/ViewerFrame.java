package graphviewer;

import java.awt.*;
import javax.swing.*;

public class ViewerFrame extends JFrame {
    private ImageComponent imageComponent;

    public ViewerFrame(String title) {
        setTitle(title);
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();

        int screenWidth = (int) (screenSize.width * 0.7);
        int screenHeight = (int) (screenSize.height * 0.7);
        setSize(screenWidth, screenHeight);

        ImageIcon imageIcon = new ImageIcon("icon.jpg");
        setIconImage(imageIcon.getImage());

        initImageComponent();
    }

    private void initImageComponent() {
        imageComponent = new ImageComponent();
        imageComponent.setSize(getWidth(), getHeight());
        imageComponent.setFocusable(true);
        JScrollPane scroll = new JScrollPane(imageComponent);
        add(scroll);
    }

    public void loadImage(byte[] imgData) {
        imageComponent.loadImage(imgData, new Dimension(getWidth() - 50, getHeight() - 200));
        imageComponent.revalidate();
        imageComponent.repaint();
    }

}
