import graphviewer.ViewerFrame;

import javax.swing.*;

public abstract class GraphRenderer {

    private boolean enabled = true;

    /**
     * Rendert einen Graphen und gibt das PNG-Bild alt Byte-Array zurück
     *
     * @param g zu rendernder Graph
     * @return PNG-Bild als Byte-Array
     */
    abstract byte[] renderGraph(Graph g);

    /**
     * Rendert einen Graphen und zeigt ihn in einem neuen Fenster an.
     *
     * @param g     zu rendernder Graph
     * @param title Titel des Fensters
     */
    public void renderGraphAndShow(Graph g, String title) {
        if (!enabled) {
            return;
        }
        byte[] imgData = renderGraph(g);
        ViewerFrame vf = createWindow(title);
        showGraphImage(vf, imgData);
    }

    /**
     * Rendert einen Graphen und zeigt ihn in im übergebenen Fenster vf an.
     *
     * @param vf        Fenster, in dem der Graph angezeigt werden soll
     * @param g         zu rendernderer Graph
     * @param delayInMs Zeit in ms, die vor der Anzeige des Graphen gewartet werden soll.
     */
    public void renderGraphAndShow(ViewerFrame vf, Graph g, int delayInMs) {
        if (!enabled) {
            return;
        }
        byte[] imgData = renderGraph(g);
        try {
            Thread.sleep(delayInMs);
        } catch (InterruptedException ignored) {
        }
        showGraphImage(vf, imgData);
    }

    /**
     * Erzeugt ein neues Fenster.
     *
     * @param title Titel des neuen Fensters
     * @return neu erzeugtes Fenster
     */
    public ViewerFrame createWindow(String title) {
        if (!enabled) {
            return null;
        }
        ViewerFrame vf = new ViewerFrame(title);
        vf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vf.setLocationByPlatform(true);
        vf.setVisible(true);

        return vf;
    }

    /**
     * Zeigt das übergebene Bild im übergebenen Fenster an.
     *
     * @param vf      Fenster
     * @param imgData anzuzeigendes Bild
     */
    private void showGraphImage(ViewerFrame vf, byte[] imgData) {
        vf.loadImage(imgData);
        vf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        if (!vf.isVisible()) {
            vf.setLocationByPlatform(true);
            vf.setVisible(true);
        }
    }

    /**
     * Generiert eine textuelle Repräsentation des übergebenen Graphen im dot-Format.
     *
     * @param g unzuwandelnder Graph
     * @return Repräsentation des Graphen g im dot-Format
     */
    String generateDotFormatedGraph(Graph g) {
        StringBuilder sb = new StringBuilder();
        List<Edge> edges = g.getEdges();

        sb.append("graph { \n");

        List<Vertex> vertices = g.getVertices();
        vertices.toFirst();
        while (vertices.hasAccess()) {
            Vertex currentVertex = vertices.getContent();
            if (currentVertex instanceof ColorVertex) {
                sb.append(String.format(" \"%s\" [style=\"filled\", fillcolor=\"%s\"]%n",
                        currentVertex.getID(),
                        calcFillcolor((ColorVertex) currentVertex))
                );
            }
            sb.append(String.format(" \"%s\" [color=\"%s\"]%n", currentVertex.getID(), calcColor(currentVertex)));
            vertices.next();
        }
        sb.append(String.format("%n"));

        edges.toFirst();
        while (edges.hasAccess()) {
            Edge currentEdge = edges.getContent();
            Vertex vertexA = currentEdge.getVertices()[0];
            Vertex vertexB = currentEdge.getVertices()[1];
            String edgeRepresentationInDotFormat = String.format(
                    "   \"%s\" -- \"%s\" [weight=%s, label=%s, color=%s]%n",
                    vertexA.getID(),
                    vertexB.getID(),
                    currentEdge.getWeight(),
                    currentEdge.getWeight(),
                    calcColor(currentEdge)
            );
            sb.append(edgeRepresentationInDotFormat);
            edges.next();
        }

        sb.append("}");

        return sb.toString();
    }

    /**
     * Gibt die Farbe für eine Kante basierend auf ihrer Markierung zurück.
     *
     * @param e Kante zu welcher die Farbe bestimmt werden soll.
     * @return Farbe der Kante
     */
    private String calcColor(Edge e) {
        return e.isMarked() ? "red" : "black";
    }

    /**
     * Gibt die Farbe für einen Knoten basierend auf seiner Markierung zurück.
     *
     * @param v Knoten zu welchem die Farbe bestimmt werden soll.
     * @return Farbe des Knotens
     */
    private String calcColor(Vertex v) {
        return v.isMarked() ? "red" : "black";
    }

    /**
     * Gibt die Farbe für einen ColorVertex basierend auf seiner Färbung zurück.
     *
     * @param v Knoten zu welchem die Farbe bestimmt werden soll.
     * @return Farbe des Knotens
     */
    private String calcFillcolor(ColorVertex v) {
        return v.getColor() != null ? v.getColor() : "white";
    }

    /**
     * Methode zum Deaktivieren des Renderings.
     *
     * @param pEnabled true: Rendering angeschaltet. false: Rendering ausgeschaltet.
     */
    public void setEnabled(boolean pEnabled) {
        enabled = pEnabled;
    }
}
