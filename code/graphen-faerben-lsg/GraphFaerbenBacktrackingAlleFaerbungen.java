import graphviewer.ViewerFrame;

public class GraphFaerbenBacktrackingAlleFaerbungen {

    private static final String[] FARBEN = {"green", "pink", "yellow", "blue", "purple"};

    private final GraphRenderer gr;
    private final ViewerFrame vf;
    private final GraphPrinter gp;

    private int anzahlFaerbungen = 0;
    private final List<Graph> gefundeneFaerbungen = new List<>();

    /**
     * Konstruktor der Algorithmus-Klasse.
     *
     * @param title Titel, der für das Fenster ViewerFrame vf gesetzt wird.
     */
    public GraphFaerbenBacktrackingAlleFaerbungen(String title) {
        gp = new GraphPrinter();

        gr = new LocalGraphRenderer();
        //gr.setEnabled(false); // zum Rendern auskommentieren

        vf = gr.createWindow(title);
    }

    /**
     * Versucht einen Graphen mit der Anzahl k der übergebenen Farben zu färben.
     * Hierfür wird ein Backtracking-Algorithmus eingesetzt. Dieser testet systematisch alle möglichen Kombinationen.
     *
     * @param g              zu färbender Graph
     * @param maximaleFarben Anzahl der maximal zu verwendenden Farben
     */
    public void graphFaerben(Graph g, int maximaleFarben) {
        List<Vertex> allVerticesList = g.getVertices();
        allVerticesList.toFirst();
        ColorVertex startknoten = (ColorVertex) allVerticesList.getContent();

        graphFaerbenRekursiv(g, allVerticesList, startknoten, maximaleFarben);
        System.out.printf("Es wurden %s Färbungen gefunden.", anzahlFaerbungen);

        int aktuelleFaerbung = 0;
        gefundeneFaerbungen.toFirst();
        while (gefundeneFaerbungen.hasAccess()) {
            gr.renderGraphAndShow(gefundeneFaerbungen.getContent(), "Färbung: " + aktuelleFaerbung);
            aktuelleFaerbung++;
            gefundeneFaerbungen.next();
        }
    }

    /**
     * Interne Backtracking-Methode zum systematischen Testen der Färbungen.
     * Alle gefundenen Färbungen werden in der Liste gefundeneFaerbungen gespeichert.
     *
     * @param g               zu färbender Graph
     * @param alleKnoten      Liste mit allen Knoten des Graphen
     * @param aktuellerKnoten aktuell zu betrachtender Knoten aus alleKnoten
     * @param maximaleFarben  maximale Anzahl k der zu verwendenden Farben
     */
    private void graphFaerbenRekursiv(Graph g, List<Vertex> alleKnoten, ColorVertex aktuellerKnoten, int maximaleFarben) {
        for (int i = 0; i < maximaleFarben; i++) {
            String aktuelleFarbe = FARBEN[i];
            aktuellerKnoten.setColor(aktuelleFarbe);
            aktuellerKnoten.setMark(true);
            //gr.renderGraphAndShow(vf, g, 0);
            if (isFarbeMoeglich(g, aktuellerKnoten)) {
                if (g.allVerticesMarked()) {
                    Graph clone = GraphUtils.deepCopy(g);
                    gefundeneFaerbungen.append(clone);
                    anzahlFaerbungen++;
                } else {
                    ColorVertex nextVertex = getNextVertex(alleKnoten, aktuellerKnoten);
                    graphFaerbenRekursiv(g, alleKnoten, nextVertex, maximaleFarben);
                }
            }
            // Backtracking: Letzte Färbung rückgängig machen
            aktuellerKnoten.setColor(null);
            aktuellerKnoten.setMark(false);
            //gr.renderGraphAndShow(vf, g, 0);
        }
    }

    /**
     * Gibt den nachfolgenden Knoten zum übergebenen Knoten v aus der Liste der Knoten zurück.
     *
     * @param allVertices Liste aller Knoten
     * @param v           Knoten, für den der Nachfolger gesucht wird
     * @return Nachfolger von v. Fall nicht vorhanden null.
     */
    private ColorVertex getNextVertex(List<Vertex> allVertices, ColorVertex v) {
        allVertices.toFirst();
        while (allVertices.getContent() != v) {
            allVertices.next();
        }
        allVertices.next();
        if (!allVertices.hasAccess()) {
            return null;
        }
        return (ColorVertex) allVertices.getContent();
    }

    /**
     * Prüft, ob der übergebene Knoten v eine gültige Färbung besitzt.
     *
     * @param g zu förbender Graph
     * @param v zu prüfender Knoten
     * @return true, falls Färbung gültig. false, sonst.
     */
    private boolean isFarbeMoeglich(Graph g, ColorVertex v) {
        List<Vertex> nachbarknoten = g.getNeighbours(v);
        boolean isFaerbungGueltig = true;
        nachbarknoten.toFirst();
        while (nachbarknoten.hasAccess()) {
            ColorVertex aktuellerNachbar = (ColorVertex) nachbarknoten.getContent();
            if (v.getColor().equals(aktuellerNachbar.getColor())) {
                isFaerbungGueltig = false;
            }
            nachbarknoten.next();
        }
        return isFaerbungGueltig;
    }

}
